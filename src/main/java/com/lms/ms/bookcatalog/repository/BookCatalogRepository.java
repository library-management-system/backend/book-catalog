package com.lms.ms.bookcatalog.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.pmw.tinylog.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.ms.bookcatalog.MongoConfig;
import com.lms.ms.bookcatalog.entity.Book;
import com.lms.ms.bookcatalog.exception.BookNotFoundException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertOneResult;

@Component
public class BookCatalogRepository {

	@Autowired
	MongoConfig config;

	ObjectMapper objectMapper = new ObjectMapper();

	public Book get(String id) {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		FindIterable<Document> result = collection.find(Filters.eq("_id", Long.parseLong(id)));
		Map<String, Object> book = result.first();
		if (book == null) {
			throw new BookNotFoundException("the book is not found with id : " + id);
		}
		book.remove("_id");
		return objectMapper.convertValue(book, Book.class);
	}

	public Book create(Book book) {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		Map<String, Object> bookMap = objectMapper.convertValue(book, Map.class);
		bookMap.put("_id", book.getId());
		InsertOneResult insertResult = collection.insertOne(new Document(bookMap));
		if (insertResult.wasAcknowledged()) {
			Logger.info("book successfully inserted");
		}
		return objectMapper.convertValue(book, Book.class);
	}

	public void update(Book book) {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		Map<String, Object> bookMap = objectMapper.convertValue(book, Map.class);
		Document result = collection.findOneAndReplace(Filters.eq("_id", book.getId()), new Document(bookMap));
		if (result == null) {
			throw new BookNotFoundException("The book is trying to update is not found ");
		}
		Logger.info("book updated succesfully");
	}

	public void delete(String id) {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		Document result = collection.findOneAndDelete(Filters.eq("_id", id));
		if (result == null) {
			throw new BookNotFoundException("the book is not found with id : " + id);
		}
		Logger.info("book deleted succesfully");
	}

	public List<Book> search(String text) {
		MongoClient mongoClient = config.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase("lms");
		MongoCollection<Document> collection = database.getCollection("book-catalog");
		FindIterable<Document> results = collection.find(Filters.text(text));
		MongoCursor<Document> iterator = results.iterator();
		List<Book> books = new ArrayList<>();
		while (iterator.hasNext()) {
			Map<String, Object> book = iterator.next();
			book.remove("_id");
			books.add(objectMapper.convertValue(book, Book.class));
		}
		return books;
	}

}
