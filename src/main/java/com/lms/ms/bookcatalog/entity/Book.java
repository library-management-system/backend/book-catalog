package com.lms.ms.bookcatalog.entity;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class Book {

	private long isbn;
	private String category;
	private String title;
	private String publisher;
	private List<String> authors;
	private String language;
	private BigDecimal price;
	private long dataOfPurchase;

	public long getId() {
		return isbn;
	}

	public void setId(long id) {
		this.isbn = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public long getDataOfPurchase() {
		return dataOfPurchase;
	}

	public void setDataOfPurchase(long dataOfPurchase) {
		this.dataOfPurchase = dataOfPurchase;
	}

}
