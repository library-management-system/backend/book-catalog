package com.lms.ms.bookcatalog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lms.ms.bookcatalog.entity.Book;
import com.lms.ms.bookcatalog.repository.BookCatalogRepository;

@Component
public class BookCatalogService {

	@Autowired
	BookCatalogRepository bookCatalogRepository;

	public Book getBook(String id) {
		return bookCatalogRepository.get(id);
	}

	public void updateBook(Book book) {
		bookCatalogRepository.update(book);
	}

	public void createBook(Book book) {
		bookCatalogRepository.create(book);
	}

	public void deleteBook(String id) {
		bookCatalogRepository.delete(id);
	}
	
	public List<Book> search(String text) {
		return bookCatalogRepository.search(text);
	}
}
