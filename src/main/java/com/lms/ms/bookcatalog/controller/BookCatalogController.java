package com.lms.ms.bookcatalog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.ms.bookcatalog.entity.Book;
import com.lms.ms.bookcatalog.service.BookCatalogService;

@RestController
@RequestMapping("/book-catalog")
public class BookCatalogController {

	@Autowired
	BookCatalogService bookCatalogService;

	@GetMapping("/book/{id}")
	public ResponseEntity<Book> getBook(@PathVariable("id") String bookId) {
		Book book = bookCatalogService.getBook(bookId);
		return new ResponseEntity<Book>(book, HttpStatus.OK);
	}

	@PutMapping("/book/{id}")
	public ResponseEntity<HttpStatus> modifyBook(@RequestBody Book book) {
		bookCatalogService.updateBook(book);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/book/{id}")
	public ResponseEntity<HttpStatus> deleteBook(@PathVariable("id") String bookId) {
		bookCatalogService.deleteBook(bookId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/book/{id}")
	public ResponseEntity<HttpStatus> createBook(@RequestBody Book book) {
		bookCatalogService.createBook(book);
		return new ResponseEntity<>(HttpStatus.CREATED);

	}

	@GetMapping("/search/book/{text}")
	public ResponseEntity<List<Book>> search(@PathVariable("text") String searchText) {
		List<Book> books = bookCatalogService.search(searchText);
		return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
	}
}
